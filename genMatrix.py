from random import randint

def printMatrix(x):
    for i in x:
        for j in i:
            print("%3d" % j, end=' ')
        print()


def genMatrix(n, todo=0):       #argument is dimension nxn of square matrix and what to do with that matrix
    x = []
    for i in range(n):
        y = []
        for j in range(n):
           y.append(randint(-99,99))
           #y.append(0)
        x.append(y)
    if todo == 1:
        print(x)
    elif todo == "pretty":
        printMatrix(x)
    else:
        return x

if __name__ == "__main__":
    genMatrix(500,1)
