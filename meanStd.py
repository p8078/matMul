def meanStd(array):
    mean = 0.
    std = 0.
    n = len(array)
    if n == 0:
        return [0, 0]
    for i in array:
        mean += i
    mean /= len(array)
    for i in array:
        std += (i - mean) ** 2
    std /= len(array)
    return [mean, std]

def main():
    a = [3, 4, 3]
    print(meanStd(a))


if __name__ == "__main__":
    main()
