import timeit
import numpy as np
from genMatrix import printMatrix
from matrixHome import returnMatrix
from meanStd import meanStd

def matrixMul(a,b,size):
    start = timeit.default_timer()
    z = np.matmul(a,b)
    return timeit.default_timer() - start

def main():
    [a,b] = returnMatrix()
    n = len(a[0])
    t = []                      #array to keep tab of the timing
    start = timeit.default_timer()
    i = 0
    while i < 50:
        time1 = matrixMul(a,b,n)
        if timeit.default_timer() - start > 180:   #throwing away first 3 minutes of data
            i += 1
            t.append(time1)
    print(t)
    [mean, std] = meanStd(t)
    print("Mean:  ", mean)
    print("Std:   ", std)



if __name__ == "__main__":
    main()
